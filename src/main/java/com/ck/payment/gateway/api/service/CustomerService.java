package com.ck.payment.gateway.api.service;


import com.ck.payment.gateway.api.responses.ErrorResponseExtended;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ck.payment.gateway.api.entities.Customer;
import com.ck.payment.gateway.api.repositories.CustomerRepository;
import com.ck.payment.gateway.api.responses.CheckCustomerResponse;
import com.ck.payment.gateway.api.responses.ErrorResponse;

@Service
public class CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    public ResponseEntity checkCustomerStatus(Long checkToken, String agreementId, String globalReference) {
        ResponseEntity<?> response = null;


        Customer customer = customerRepository.findByDealerIdAndMobileNo(checkToken, agreementId);


        if (customer != null) {
            CheckCustomerResponse checkCusResponse = new CheckCustomerResponse();

            checkCusResponse.setStatus(customer.getStatus());
            checkCusResponse.setResponseCode("00");
            checkCusResponse.setGlobalReference(globalReference);
            checkCusResponse.setAgreementId(agreementId);
            HttpHeaders responseHeader = new HttpHeaders();
            response = new ResponseEntity<>(checkCusResponse, responseHeader, HttpStatus.CREATED);

            return response;
        }

        ErrorResponseExtended errorResponseExtended = new ErrorResponseExtended();
        errorResponseExtended.setErrors(null);
        errorResponseExtended.setMessage("Customer not found");
        errorResponseExtended.setErrorCode("01");
        errorResponseExtended.setGlobalReference(globalReference);
        errorResponseExtended.setAgreementId(agreementId);
        HttpHeaders responseHeader = new HttpHeaders();
        return new ResponseEntity<>(errorResponseExtended, responseHeader, HttpStatus.ACCEPTED);
    }
}

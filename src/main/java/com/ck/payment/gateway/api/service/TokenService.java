package com.ck.payment.gateway.api.service;


import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;

import com.ck.payment.gateway.api.dto.RefreshTokenDto;
import com.ck.payment.gateway.api.entities.Dealer;
import com.ck.payment.gateway.api.entities.IssuedToken;
import com.ck.payment.gateway.api.enums.GrantTypes;
import com.ck.payment.gateway.api.repositories.DealerRepository;
import com.ck.payment.gateway.api.repositories.IssuedTokensRepository;
import com.ck.payment.gateway.api.responses.ErrorResponse;
import com.ck.payment.gateway.api.responses.TokenResponse;

@Service
public class TokenService {
    private Logger logger = LoggerFactory.getLogger(TokenService.class);
    @Autowired
    DealerRepository dealerRepository;
    @Autowired
    IssuedTokensRepository issuedTokensRepository;


    private Random rnd = new Random();


    public ResponseEntity refreshToekn(HttpServletRequest request, RefreshTokenDto refreshTokenDto,
                                          Errors validatorErrors) {

        if (validatorErrors.hasErrors()) {
            Map<String, String> errors = new HashMap<>();
            validatorErrors.getAllErrors().forEach(error -> {
                String fieldName = ((FieldError) error).getField();
                String errorMessage = error.getDefaultMessage();
                errors.put(fieldName, errorMessage);
            });
            ErrorResponse errorResponse = new ErrorResponse();
            errorResponse.setErrors(errors);
            errorResponse.setMessage("Input fields errors");
            errorResponse.setErrorCode("ex-0100");
            return ResponseEntity.badRequest().body(errorResponse);
        }

        ResponseEntity<?> response = null;
        Optional<Dealer> existDealer = null;
        if (refreshTokenDto.getGrantType().equals(GrantTypes.refresh_token)) {
            IssuedToken oldToken = issuedTokensRepository.findByRefreshToken(refreshTokenDto.getRefreshToken());
            if (oldToken == null) {
                ErrorResponse errorResponse = new ErrorResponse();
                errorResponse.setErrors(null);
                errorResponse.setMessage("Invalid refresh token");
                errorResponse.setErrorCode("ex-0104");
                return ResponseEntity.badRequest().body(errorResponse);
            }
            existDealer = dealerRepository.findById(oldToken.getDealerId());
        } else {
            existDealer = dealerRepository.findbyUserNameAndPassword(refreshTokenDto.getUsername(),
                    refreshTokenDto.getPassword());
        }
        if (existDealer.isPresent()) {
            Dealer dealer = existDealer.get();
            String requestAuthorizationCode = request.getHeader("Authorization");
            if (!checkAuthorizationCode(requestAuthorizationCode, dealer)) {
                ErrorResponse errorResponse = new ErrorResponse();
                errorResponse.setErrors(null);
                errorResponse.setMessage("Invalid request");
                errorResponse.setErrorCode("ex-0101");
                return ResponseEntity.badRequest().body(errorResponse);
            }

            TokenResponse tokenResponse = new TokenResponse();
            tokenResponse.setTokenType("bearer");
            tokenResponse.setExpiresIn(dealer.getSessionTimeOut());

            IssuedToken issuedToken = generateToken(dealer, refreshTokenDto.getGrantType());

            tokenResponse.setRefreshToken(issuedToken.getRefreshToken());
            tokenResponse.setAccessToken(issuedToken.getAccessToken());

            HttpHeaders responseHeader = new HttpHeaders();
            responseHeader.set("Authorization", "Bearer " + issuedToken.getAccessToken());
            response = new ResponseEntity<>(tokenResponse, responseHeader, HttpStatus.CREATED);
        } else {
            ErrorResponse errorResponse = new ErrorResponse();
            errorResponse.setErrors(null);
            errorResponse.setMessage("Invalid credentials");
            errorResponse.setErrorCode("ex-0103");
            HttpHeaders responseHeader = new HttpHeaders();
            response = new ResponseEntity<>(errorResponse, responseHeader, HttpStatus.ACCEPTED);
        }
        return response;
    }

    private boolean checkAuthorizationCode(String requestAuthorizationCode, Dealer dealer) {
        String baseCode = dealer.getConsumerKey() + ":" + dealer.getConsumerSecret();

        String authorizationCode = "Basic " + Base64.getEncoder().encodeToString(baseCode.getBytes());
        logger.info("Request code : {} system code : {}" , requestAuthorizationCode, authorizationCode);
        return requestAuthorizationCode.equals(authorizationCode);
    }

    private IssuedToken generateToken(Dealer dealer, GrantTypes grantType) {
        IssuedToken issuedToken = new IssuedToken();
        IssuedToken oldToken = issuedTokensRepository.findByDealerId(dealer.getId());
        Calendar cal = Calendar.getInstance();

        if (oldToken == null) { // no issued tokens
            issuedToken.setDealerId(dealer.getId());
            issuedToken.setAccessToken(uniqueString());
            issuedToken.setRefreshToken(uniqueString());
            Date now = new Date();
            issuedToken.setIssuedAt(now);

            cal.setTime(now);
            cal.add(Calendar.SECOND, dealer.getSessionTimeOut());

            issuedToken.setExpireAt(cal.getTime());
            issuedToken.setSessionTimeout(dealer.getSessionTimeOut());

            issuedTokensRepository.save(issuedToken);

            logger.info("Generated token => {}", issuedToken);
        } else {
            Date now = new Date();
            if (grantType.equals(GrantTypes.refresh_token) || now.compareTo(oldToken.getExpireAt()) > 0) { // token expired, refreshing

                oldToken.setAccessToken(uniqueString());
                oldToken.setRefreshToken(uniqueString());
                oldToken.setIssuedAt(now);

                cal.setTime(now);
                cal.add(Calendar.SECOND, dealer.getSessionTimeOut());

                oldToken.setExpireAt(cal.getTime());
                oldToken.setSessionTimeout(dealer.getSessionTimeOut());

                issuedTokensRepository.save(oldToken);
                if (grantType.equals(GrantTypes.refresh_token)) {
                    logger.info("Refreshed tokens due grant type is  refresh_token => {}", oldToken);
                } else {
                    logger.info("Updated tokens due to token expired => {}", oldToken);
                }

                return oldToken;
            }

            logger.info("Token not expired");
            return oldToken;
        }

        return issuedToken;
    }

    private String uniqueString() {
        boolean validToken = false;
        String generatedString = "";

        while (!validToken) {
            String validChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
            StringBuilder salt = new StringBuilder();
            while (salt.length() < 200) { // length of the random string.
                int index = (int) (rnd.nextFloat() * validChars.length());
                salt.append(validChars.charAt(index));
            }
            generatedString = salt.toString();
            IssuedToken oldToken = issuedTokensRepository.checkForUniqueCode(generatedString);

            if (oldToken == null) {
                validToken = true;
            }
        }

        return generatedString;
    }


    // if valid token will return dealer id
//	if -1 = token expired
//	if 0 = invalid token
    public Long chekcBearerToken(String requestBearerToken) {
        String[] splitTokens = requestBearerToken.split(" ");
        if (splitTokens.length == 2) {
            String splitToken = splitTokens[1];

            IssuedToken issuedToken = issuedTokensRepository.findByAccessToken(splitToken);
            Date now = new Date();
            if (issuedToken != null) {
                if (now.compareTo(issuedToken.getExpireAt()) > 0) { // token expired
                    return (long) -1;
                }

                return issuedToken.getDealerId();
            }
        }

        return (long) 0;
    }

}

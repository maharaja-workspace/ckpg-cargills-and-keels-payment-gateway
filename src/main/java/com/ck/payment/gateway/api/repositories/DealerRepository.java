package com.ck.payment.gateway.api.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ck.payment.gateway.api.entities.Dealer;

@Repository
public interface DealerRepository extends CrudRepository<Dealer, Long>{
	
	@Query(value = "SELECT * FROM Dealer WHERE user_name = :userName AND password = :password LIMIT 1", nativeQuery = true)
	Optional<Dealer> findbyUserNameAndPassword(String userName, String password);

}

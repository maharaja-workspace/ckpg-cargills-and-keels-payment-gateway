package com.ck.payment.gateway.api.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import com.ck.payment.gateway.api.responses.ErrorResponseExtended;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.ck.payment.gateway.api.dto.DealerPaymentDto;
import com.ck.payment.gateway.api.responses.ErrorResponse;
import com.ck.payment.gateway.api.service.DealerPaymentService;
import com.ck.payment.gateway.api.service.TokenService;

@RestController
public class DealerPaymentController {
	Logger logger = LoggerFactory.getLogger(DealerPaymentController.class);
	@Autowired TokenService tokenService ;
	@Autowired DealerPaymentService dealerPaymentService;

	@PostMapping(value="/dealer/payment")
	public ResponseEntity dealerPayment(
			@RequestHeader(value="Authorization", required = true) String requestBearerToken,
			@Valid @RequestBody DealerPaymentDto dealerPaymentDto,
			Errors validatorErrors){
		ErrorResponse errorResponse = new ErrorResponse();
		ErrorResponseExtended errorResponseExtended = new ErrorResponseExtended();
		try {
		logger.info("Recieved dealerPayment with => {}", dealerPaymentDto);
		Long checkToken = tokenService.chekcBearerToken(requestBearerToken);

		if(checkToken == -1) {
			errorResponse.setErrors(null);
			errorResponse.setMessage("Token expired");
			errorResponse.setErrorCode("03");
			return ResponseEntity.badRequest().body(errorResponse);
		}else if(checkToken == 0) { // invalid token

			errorResponse.setErrors(null);
			errorResponse.setMessage("Invalid request");
			errorResponse.setErrorCode("03");
			return ResponseEntity.badRequest().body(errorResponse);
		}
		
		if (validatorErrors.hasErrors()) {
			Map<String, String> errors = new HashMap<>();
			validatorErrors.getAllErrors().forEach(error -> {
				String fieldName = ((FieldError) error).getField();
				String errorMessage = error.getDefaultMessage();
				errors.put(fieldName, errorMessage);
			});

			errorResponseExtended.setErrors(errors);
			errorResponseExtended.setMessage("Input fields errors");
			errorResponseExtended.setErrorCode("01");
			errorResponseExtended.setAgreementId(dealerPaymentDto.getAgreementId());
			errorResponseExtended.setGlobalReference(dealerPaymentDto.getGlobalReference());
			return ResponseEntity.badRequest().body(errorResponseExtended);
		}
            dealerPaymentDto.setDealerId(checkToken);
		return dealerPaymentService.dealerPayment(dealerPaymentDto);
		
	} catch (Exception e) {
		errorResponse.setErrors(null);
		errorResponse.setMessage("Server error. Please try again");
		errorResponse.setErrorCode("02");
			logger.error("Error in customerStatus for => {} Error => {}", dealerPaymentDto, e.getLocalizedMessage());
		return ResponseEntity.badRequest().body(errorResponse);
	}
	}
}

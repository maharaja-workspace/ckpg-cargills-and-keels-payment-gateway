package com.ck.payment.gateway.api.responses;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.ck.payment.gateway.api.enums.Status;

public class CheckCustomerResponse {

	@Enumerated(EnumType.STRING)
	private Status status;

	private String responseCode;

	private String globalReference;

	private String agreementId;

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getGlobalReference() {
		return globalReference;
	}

	public void setGlobalReference(String globalReference) {
		this.globalReference = globalReference;
	}

	public String getAgreementId() {
		return agreementId;
	}

	public void setAgreementId(String agreementId) {
		this.agreementId = agreementId;
	}
}

package com.ck.payment.gateway.api.repositories;

import org.springframework.data.repository.CrudRepository;

import com.ck.payment.gateway.api.entities.DealerPayment;

public interface DealerPaymentRepository extends CrudRepository<DealerPayment, Long>{

}

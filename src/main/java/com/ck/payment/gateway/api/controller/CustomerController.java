package com.ck.payment.gateway.api.controller;

import javax.validation.*;

import com.ck.payment.gateway.api.responses.ErrorResponseExtended;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.ck.payment.gateway.api.dto.CheckCustomerStatusDto;
import com.ck.payment.gateway.api.responses.ErrorResponse;
import com.ck.payment.gateway.api.service.CustomerService;
import com.ck.payment.gateway.api.service.TokenService;

import java.util.HashMap;
import java.util.Map;

@Controller
public class CustomerController {
	private Logger logger = LoggerFactory.getLogger(CustomerController.class);
	@Autowired CustomerService customerService;
	@Autowired TokenService tokenService ;

	@PostMapping(value = "customer/status")
	public ResponseEntity checkCustomerStatus(
			@RequestHeader(value="Authorization", required = true) String requestBearerToken,
			@Valid @RequestBody CheckCustomerStatusDto checkCustomerStatusDto,
			Errors validatorErrors
			) {
		ErrorResponseExtended errorResponseExtended = new ErrorResponseExtended();
		ErrorResponse errorResponse = new ErrorResponse();
		try {
			logger.info("Start checkCustomerStatus with => {}" , checkCustomerStatusDto);
			Long checkToken = tokenService.chekcBearerToken(requestBearerToken);
			if(checkToken == -1) {

				errorResponse.setErrors(null);
				errorResponse.setMessage("Token expired");
				errorResponse.setErrorCode("03");
				return ResponseEntity.badRequest().body(errorResponse);
			}else if(checkToken == 0) { // invalid token
				errorResponse.setErrors(null);
				errorResponse.setMessage("Invalid request");
				errorResponse.setErrorCode("03");
				return ResponseEntity.badRequest().body(errorResponse);
			}
			logger.info("Dealer id => {}", checkToken);


			if(validatorErrors.hasErrors()){
				Map<String, String> errors = new HashMap<>();
				validatorErrors.getAllErrors().forEach(error -> {
					String fieldName = ((FieldError) error).getField();
					String errorMessage = error.getDefaultMessage();
					errors.put(fieldName, errorMessage);
				});
				errorResponseExtended.setErrors(errors);
				errorResponseExtended.setMessage("Input fields errors");
				errorResponseExtended.setErrorCode("01");
				errorResponseExtended.setAgreementId(checkCustomerStatusDto.getAgreementId());
				errorResponseExtended.setGlobalReference(checkCustomerStatusDto.getGlobalReference());
				return ResponseEntity.badRequest().body(errorResponseExtended);
			}

			return customerService.checkCustomerStatus(checkToken, checkCustomerStatusDto.getAgreementId(), checkCustomerStatusDto.getGlobalReference());
		} catch (Exception e) {
			errorResponse.setErrors(null);
			errorResponse.setMessage("Server error. Please try again");
			errorResponse.setErrorCode("02");
			logger.error("Error in customerStatus for => {} Error => {}" , checkCustomerStatusDto, e.getLocalizedMessage());
			return ResponseEntity.badRequest().body(errorResponse);
		}
	}
	
	
}

package com.ck.payment.gateway.api.service;

import java.util.Calendar;

import com.ck.payment.gateway.api.responses.ErrorResponseExtended;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ck.payment.gateway.api.dto.DealerPaymentDto;
import com.ck.payment.gateway.api.entities.Customer;
import com.ck.payment.gateway.api.entities.DealerPayment;
import com.ck.payment.gateway.api.repositories.CustomerRepository;
import com.ck.payment.gateway.api.repositories.DealerPaymentRepository;
import com.ck.payment.gateway.api.responses.ErrorResponse;

@Service
public class DealerPaymentService {
	private Logger logger = LoggerFactory.getLogger(DealerPaymentService.class);
	@Autowired DealerPaymentRepository dealerPaymentRepository;
	@Autowired CustomerRepository customerRepository;
	
	public ResponseEntity  dealerPayment(DealerPaymentDto dealerPaymentDto){

		logger.info("Start dealerPayment with => {}", dealerPaymentDto);
		Customer customer = customerRepository.findByMobileNoAndDealerId(dealerPaymentDto.getAgreementId(), dealerPaymentDto.getDealerId());
		
		if(customer == null) {
			ErrorResponseExtended errorResponseExtended = new ErrorResponseExtended();
			errorResponseExtended.setErrors(null);
			errorResponseExtended.setMessage("Customer not found");
			errorResponseExtended.setErrorCode("01");
			errorResponseExtended.setAgreementId(dealerPaymentDto.getAgreementId());
			errorResponseExtended.setGlobalReference(dealerPaymentDto.getGlobalReference());
			HttpHeaders responseHeader = new HttpHeaders();
			return new ResponseEntity<>(errorResponseExtended, responseHeader, HttpStatus.ACCEPTED);
		}
		
		DealerPayment dealerPayment = new DealerPayment();
		
		dealerPayment.setAmount(dealerPaymentDto.getAmount());
		
		Calendar cal = Calendar.getInstance();	
		dealerPayment.setCreatedDate(cal.getTime());
		
		dealerPayment.setMobileno(dealerPaymentDto.getAgreementId());
		dealerPayment.setCustomerId(customer.getId());
		dealerPayment.setGlobalReference(dealerPaymentDto.getGlobalReference());
		dealerPaymentRepository.save(dealerPayment);

		dealerPaymentDto.setResponseCode("00");
		return ResponseEntity.accepted().body(dealerPaymentDto);
	}

}

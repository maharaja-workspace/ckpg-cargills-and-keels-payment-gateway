package com.ck.payment.gateway.api.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dealer_payments")
public class DealerPayment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String mobileno;
    private long customerId;
    private double amount;
    private Date createdDate;
    private String globalReference;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getGlobalReference() {
        return globalReference;
    }

    public void setGlobalReference(String globalReference) {
        this.globalReference = globalReference;
    }
}

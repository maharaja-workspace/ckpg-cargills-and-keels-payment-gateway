package com.ck.payment.gateway.api.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DealerPaymentDto {

    @JsonProperty("agreementId")
    @NotBlank(message = "agreementId is mandatory")
    private String agreementId;

    @Min(value = 0, message = "Please enter valid value for amount")
    @NotNull(message = "amount is mandatory")
    private Double amount;


    private String responseCode;

    @JsonProperty("globalReference")
    @NotBlank(message = "globalReference is mandatory")
    private String globalReference;

    private Long dealerId;

    public String getAgreementId() {
        return agreementId;
    }

    public void setAgreementId(String agreementId) {
        this.agreementId = agreementId;
    }

    public Double getAmount() {
        return amount;
    }


    public void setAmount(Double amount) {
        this.amount = amount;
    }


    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getGlobalReference() {
        return globalReference;
    }

    public void setGlobalReference(String globalReference) {
        this.globalReference = globalReference;
    }

    public Long getDealerId() {
        return dealerId;
    }

    public void setDealerId(Long dealerId) {
        this.dealerId = dealerId;
    }

    @Override
    public String toString() {
        return "DealerPaymentDto{" +
                "agreementId='" + agreementId + '\'' +
                ", amount=" + amount +
                ", responseCode='" + responseCode + '\'' +
                ", globalReference='" + globalReference + '\'' +
                '}';
    }
}

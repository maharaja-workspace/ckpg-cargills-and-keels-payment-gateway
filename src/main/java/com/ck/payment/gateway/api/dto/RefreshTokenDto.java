package com.ck.payment.gateway.api.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.ck.payment.gateway.api.enums.GrantTypes;

public class RefreshTokenDto {
	
	@NotNull(message = "grant_type is mandatory")
	private GrantTypes grantType;
	
	@NotBlank(message = "username is mandatory")
	private String username;
	
	@NotBlank(message = "password is mandatory")
	private String password;
	
	private String refreshToken;

	private String responseCode;
	
	public GrantTypes getGrantType() {
		return grantType;
	}
	public void setGrantType(GrantTypes grantType) {
		this.grantType = grantType;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRefreshToken() {
		return refreshToken;
	}
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	@Override
	public String toString() {
		return "RefreshTokenDto [grantType=" + grantType + ", username=" + username + ", password=" + password
				+ ", refreshToken=" + refreshToken + "]";
	}
	

}

package com.ck.payment.gateway.api.enums;

public enum GrantTypes {
	password,
	refresh_token
}

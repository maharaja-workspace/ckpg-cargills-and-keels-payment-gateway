package com.ck.payment.gateway.api.controller;


import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ck.payment.gateway.api.dto.RefreshTokenDto;
import com.ck.payment.gateway.api.enums.GrantTypes;
import com.ck.payment.gateway.api.responses.ErrorResponse;
import com.ck.payment.gateway.api.service.TokenService;

@RestController
@RequestMapping("/apicall/token")
public class TokenController {
	private Logger logger = LoggerFactory.getLogger(TokenController.class);
	@Autowired TokenService tokenService ;
	
	@PostMapping(value="", consumes= MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ResponseEntity refreshToken(
			@RequestParam(value = "grant_type", required = true) String grantType,
			@RequestParam(value = "refresh_token", required = false) String refreshToken,
			@Valid HttpServletRequest request,
			RefreshTokenDto refreshTokenDto,
			Errors validatorErrors) {
		
		
		try {
			
			refreshTokenDto.setGrantType(GrantTypes.valueOf(grantType));
			refreshTokenDto.setRefreshToken(refreshToken);
			refreshTokenDto.setResponseCode("00");
			logger.info("Start refreshToken with => {}", refreshTokenDto);
			return tokenService.refreshToekn(
					request,
					refreshTokenDto,
					validatorErrors);
		} catch (Exception e) {
			 ErrorResponse errorResponse = new ErrorResponse();
			 errorResponse.setErrors(null);
			 errorResponse.setMessage("Server error. Please try again");
			 errorResponse.setErrorCode("02");
			 logger.error("Error in refreshToken for => {}"
			 + " Error => {}", refreshTokenDto, e.getLocalizedMessage());
			 return ResponseEntity.badRequest().body(errorResponse);
		}
	}
	
	
}

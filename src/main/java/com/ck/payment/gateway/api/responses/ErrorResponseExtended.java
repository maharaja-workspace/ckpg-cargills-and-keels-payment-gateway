package com.ck.payment.gateway.api.responses;

import java.util.Map;

public class ErrorResponseExtended {
	
	private String errorCode;
	private String message;
	private String globalReference;
	private String agreementId;
	private Map<String, String> errors;
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Map<String, String> getErrors() {
		return errors;
	}
	public void setErrors(Map<String, String> errors) {
		this.errors = errors;
	}

	public String getGlobalReference() {
		return globalReference;
	}

	public void setGlobalReference(String globalReference) {
		this.globalReference = globalReference;
	}

	public String getAgreementId() {
		return agreementId;
	}

	public void setAgreementId(String agreementId) {
		this.agreementId = agreementId;
	}
}

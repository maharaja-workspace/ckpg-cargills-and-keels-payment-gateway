package com.ck.payment.gateway.api.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ck.payment.gateway.api.entities.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long>{

	@Query(value="SELECT * FROM Customer WHERE dealer_id = :dealerId AND mobile_no = :agreementId", nativeQuery = true)
	Customer findByDealerIdAndMobileNo(Long dealerId, String agreementId);

	Customer findByMobileNoAndDealerId(String mobileNo, Long delaerId);
	

}

package com.ck.payment.gateway.api.entities;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "issued_tokens")
public class IssuedToken {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "dealer_id")
	private Long dealerId;
	
	private Date issuedAt;
	
	private Date expireAt;
	
	private int  sessionTimeout;
	
	private String refreshToken;
	
	private String accessToken;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getDealerId() {
		return dealerId;
	}

	public void setDealerId(Long dealerId) {
		this.dealerId = dealerId;
	}

	public Date getIssuedAt() {
		return issuedAt;
	}

	public void setIssuedAt(Date issuedAt) {
		this.issuedAt = issuedAt;
	}

	public Date getExpireAt() {
		return expireAt;
	}

	public void setExpireAt(Date expireAt) {
		this.expireAt = expireAt;
	}

	public int getSessionTimeout() {
		return sessionTimeout;
	}

	public void setSessionTimeout(int i) {
		this.sessionTimeout = i;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	@Override
	public String toString() {
		return "IssuedToken [id=" + id + ", dealerId=" + dealerId + ", issuedAt=" + issuedAt + ", expireAt=" + expireAt
				+ ", sessionTimeout=" + sessionTimeout + ", refreshToken=" + refreshToken + ", accessToken="
				+ accessToken + "]";
	}
	
	
	
}

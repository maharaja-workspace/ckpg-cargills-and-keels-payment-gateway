package com.ck.payment.gateway.api.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CheckCustomerStatusDto {

	@JsonProperty("agreementId")
	@NotBlank(message = "agreement id is mandatory")
	private String agreementId;

	@NotNull(message = "globalReference is mandatory")
	@NotBlank(message = "globalReference id is mandatory")
	private String globalReference;

	@Min(value = 0, message = "Please enter valid value for amount")
	private double amount;

	public String getAgreementId() {
		return agreementId;
	}

	public void setAgreementId(String agreementId) {
		this.agreementId = agreementId;
	}

	public String getGlobalReference() {
		return globalReference;
	}

	public void setGlobalReference(String globalReference) {
		this.globalReference = globalReference;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
}

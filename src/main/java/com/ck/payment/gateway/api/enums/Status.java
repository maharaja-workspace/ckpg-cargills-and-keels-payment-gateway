package com.ck.payment.gateway.api.enums;

public enum Status {

	activated,
	deactivated
}

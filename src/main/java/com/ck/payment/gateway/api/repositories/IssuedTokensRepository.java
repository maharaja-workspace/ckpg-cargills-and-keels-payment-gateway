package com.ck.payment.gateway.api.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ck.payment.gateway.api.entities.IssuedToken;

@Repository
public interface IssuedTokensRepository extends CrudRepository<IssuedToken, Long>{
	
	IssuedToken findByDealerId(Long dealerId);
	
	@Query(value = "SELECT * FROM issued_tokens WHERE refresh_token = :generatedString OR access_token = :generatedString LIMIT 1", nativeQuery = true)
	IssuedToken checkForUniqueCode(String generatedString);

	IssuedToken findByAccessToken(String splitToken);

	IssuedToken findByRefreshToken(String refreshToken);

}
